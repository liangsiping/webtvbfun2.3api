CREATE TABLE [dbo].[ConfigInfo] (
    [Id]                  INT            IDENTITY (1, 1) NOT NULL,
    [Name]                NVARCHAR (50)  NOT NULL,
    [UrlPrefix]           NVARCHAR (150) NOT NULL,
    [JSON_v2_3_Folder]    NVARCHAR (50)  NOT NULL,
    [EnableUploadFTP]     BIT            NOT NULL,
    [EnableUploadBak]     BIT            NOT NULL,
    [ImageUrlPrefix]     NVARCHAR (150) NOT NULL,
    [EnableUploadStorage] BIT            NOT NULL,
    CONSTRAINT [PK_ConfigInfo] PRIMARY KEY CLUSTERED ([Id] ASC)
);

SET IDENTITY_INSERT [dbo].[ConfigInfo] ON
INSERT INTO [dbo].[ConfigInfo] ([Id], [Name], [UrlPrefix], [JSON_v2_3_Folder], [EnableUploadFTP], [EnableUploadBak], [ImageUrlPrefix], [EnableUploadStorage]) VALUES (1, N'Dev', N'http://tvbdevestorage.blob.core.windows.net/', N'tvbfundev2', 0, 0, N'https://dev-tvbfun-img.tvb.com.hk/', 1)
INSERT INTO [dbo].[ConfigInfo] ([Id], [Name], [UrlPrefix], [JSON_v2_3_Folder], [EnableUploadFTP], [EnableUploadBak], [ImageUrlPrefix], [EnableUploadStorage]) VALUES (2, N'Uat', N'http://tvbdevestorage.blob.core.windows.net/', N'tvbfun2', 0, 0, N'https://dev-tvbfun-img.tvb.com.hk/', 1)
INSERT INTO [dbo].[ConfigInfo] ([Id], [Name], [UrlPrefix], [JSON_v2_3_Folder], [EnableUploadFTP], [EnableUploadBak], [ImageUrlPrefix], [EnableUploadStorage]) VALUES (3, N'Prod', N'http://tvbclouddevices.tvb.com.hk/', N'tvbfun2', 0, 0, N'https://tvbfun-img.tvb.com.hk/', 1)
SET IDENTITY_INSERT [dbo].[ConfigInfo] OFF
