﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WebTVBFun2._3Api.tvbFunApi;
using WebTVBFun2._3Api.webTvbFun2._3Api.modle;

namespace WebTVBFun2._3Api.Controllers
{
    public class TVBFunApiController : Controller
    {
        //
        // GET: /TVBFunApi/
      
        public ActionResult Index()
        {
            return View();
        }
        public string RefreshCache()
        {
            adminService adminApi = new adminService();
            return adminApi.RefreshCache();
        }

        public ActionResult GetConfigInfo()
        {
            adminService adminApi = new adminService();
            List<ConfigInfo> configInfo = adminApi.GetConfigList();
            ViewBag.configInfo = configInfo;
            return View();
        }
	}
}