﻿using Microsoft.ApplicationServer.Caching;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using TVBCOM.SDK.Database;
using Dapper;
using WebTVBFun2._3Api.webTvbFun2._3Api.common;
using WebTVBFun2._3Api.webTvbFun2._3Api.dao;
using WebTVBFun2._3Api.webTvbFun2._3Api.modle;


namespace WebTVBFun2._3Api.tvbFunApi
{
    public class adminService:DBDao
    {
        private static Dictionary<string, string> channels = new Dictionary<string, string>();
        
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                   UriTemplate = "/refreshCache")]
        public string RefreshCache()
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            StringBuilder strb = new StringBuilder();
            RefreshProgrammes();
            strb.AppendLine(RefreshSystemConfig());
            strb.AppendLine(GenerateProgrammesStaticFile());
            TaskFactory f = new TaskFactory();
            f.StartNew(delegate()
            {
                UploudGoogleCloud();
            });
            
            return strb.ToString();
        }

        /// <summary>
        /// 更新system config
        ///// </summary>
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                  UriTemplate = "/RefreshSystemConfig")]
        private string RefreshSystemConfig()
        {
            Stopwatch watch = new Stopwatch();
            StringBuilder strb = new StringBuilder();
            watch.Start();

            SystemConfigV23 systemConfigDict = null;
            Common.systemConfigList = null;
            try
            {
                SystemConfigHelper reader = new SystemConfigHelper();
                systemConfigDict =reader.getSystemConfigFromStorage();
                Common.systemConfigList = systemConfigDict;
            }
            catch (Exception ex)
            {
                strb.AppendLine(ex.StackTrace);
            }
            try
            {
                strb.AppendLine(Common.UploadFile(systemConfigDict, Common.StorageConnectionString, Common.JSON_v2_3_Folder, "systemConfig.json", "application/json", false));

            }
            catch (Exception ex)
            {
                strb.AppendLine(ex.StackTrace);
            }
            return strb.ToString();

        }

        /// <summary>
        /// 从CMS数据库获取programmelist的完整信息，并保存到list中
        /// </summary>
        /// <returns></returns>
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
               UriTemplate = "/refreshProgrammes")]
        private Object RefreshProgrammes()
        {
            try
            {
                //http://tvbdevestorage.blob.core.windows.net/
                Stopwatch watch = new Stopwatch();
                StringBuilder strb = new StringBuilder();
                watch.Start();
                String urlPrefix = Common.UrlPrefix;
                strb.AppendLine("Got imageUrlPrefix: " + watch.ElapsedMilliseconds);

                List<Dictionary<string, object>> programmes = new List<Dictionary<string, object>>();
                Dictionary<string, List<object>> optionsSet = new Dictionary<string, List<object>>();
                Dictionary<string, Dictionary<String, object>> OptionPlaceHolderSet = new Dictionary<string, Dictionary<string, object>>();

                #region channel
                using (IDbConnection conn = OpenConnection())
                {
                    channels = new Dictionary<string, string>();
                    const String sql = "select name,value from ChannelConfig where status=1";
                    using (IDataReader readerChannel = conn.ExecuteReader(sql, CommandType.Text))
                    {
                        while (readerChannel.Read())
                        {
                            channels.Add(readerChannel["name"].ToString(), readerChannel["value"].ToString());
                        }
                    }
                }
                #endregion

                #region optionInfo
                using (IDbConnection conn = OpenConnection())
                {
                    watch.Restart();
                    using (IDataReader reader = conn.ExecuteReader("select * from OptionInfoView order by programmeId desc,sortOrder asc", CommandType.Text))
                    {
                        while (reader.Read())
                        {
                            List<object> list = null;
                            Dictionary<String, object> dict = ProgrammeHelper.parseDictionaryFromReader(reader, urlPrefix, channels);
                            if (!optionsSet.TryGetValue(Convert.ToString(reader["programmeId"]), out list))
                            {
                                list = new List<object>();
                                optionsSet.Add(Convert.ToString(reader["programmeId"]), list);
                            }
                            //把DB裡面的null，改成String.Empty
                            for (int i = 0; i < dict.Count; i++)
                            {

                                string key = dict.Keys.ElementAt(i);
                                if (key.ToLower() == "optionname"
                                    || key.ToLower() == "optionimageurl"
                                    || key.ToLower() == "optionurl"
                                    || key.ToLower() == "optionimageurlchosen"
                                    || key.ToLower() == "resultdisplayimage"
                                    || key.ToLower() == "basicauthpassword"
                                    )
                                {
                                    dict[key] = Common.CheckStringNull(dict[key]);
                                }
                                if (key.ToLower() == "optionimageurl")
                                {
                                    dict[key] = dict[key].ToString().Replace(Common.UrlPrefix, Common.ImageUrlPrefix);
                                }
                                if (key.ToLower() == "resultdisplayimage")
                                {
                                    dict[key] = dict[key].ToString().Replace(Common.UrlPrefix, Common.ImageUrlPrefix);
                                }
                                if (key.ToLower() == "optionimageurlchosen")
                                {
                                    dict[key] = dict[key].ToString().Replace(Common.UrlPrefix, Common.ImageUrlPrefix);
                                }
                            }
                            list.Add(dict);
                        }
                    }

                    strb.AppendLine("Got optionsSet: " + watch.ElapsedMilliseconds);
                }
                #endregion

                #region optionPlaceHolder
                using (IDbConnection conn = OpenConnection())
                {
                    watch.Restart();
                    using (IDataReader reader = conn.ExecuteReader("select * from OptionPlaceHolderView order by programmeId desc,sortOrder asc",
                        CommandType.Text))
                    {
                        while (reader.Read())
                        {
                            List<object> list = null;
                            Dictionary<String, object> obj = null;
                            if (!OptionPlaceHolderSet.TryGetValue(Convert.ToString(reader["programmeId"]), out obj))
                            {
                                obj = new Dictionary<string, object>();
                                list = new List<object>();
                                obj.Add("type", "");
                                obj.Add("sequence", list);
                                OptionPlaceHolderSet.Add(Convert.ToString(reader["programmeId"]), obj);
                            }
                            else
                            {
                                object _obj;
                                if (obj.TryGetValue("sequence", out _obj))
                                {
                                    list = (List<object>)_obj;
                                }
                                else
                                {
                                    list = new List<object>();
                                }
                            }
                            list.Add(Path.Combine(Common.ImageUrlPrefix, reader["optionPlaceHolderImage"].ToString()));
                        }
                    }

                    strb.AppendLine("Got OptionPlaceHolderSet: " + watch.ElapsedMilliseconds);
                }
                #endregion

                #region programmeList
                using (IDbConnection conn = OpenConnection())
                {
                    watch.Restart();
                    using (IDataReader reader = conn.ExecuteReader("GetProgrammeListWithOrder", CommandType.StoredProcedure))
                    {
                        while (reader.Read())
                        {
                            try
                            {
                                Dictionary<string, object> programme = ProgrammeHelper.parseDictionaryFromReader(reader, urlPrefix, channels);
                                String programmeId = Convert.ToString(reader["programmeId"]);
                                String alwaysSuccess = reader["alwaysSuccess"].ToString();
                                String isDisplayOptionName = reader["isDisplayOptionName"].ToString();
                                if (alwaysSuccess == "1")
                                {
                                    programme["alwaysSuccess"] = true;
                                }
                                if (alwaysSuccess == "0")
                                {
                                    programme["alwaysSuccess"] = false;
                                }
                                if (isDisplayOptionName == "1")
                                {
                                    programme["isDisplayOptionName"] = true;
                                }
                                if (isDisplayOptionName == "0")
                                {
                                    programme["isDisplayOptionName"] = false;
                                }
                                List<object> options = null;
                                if (optionsSet.TryGetValue(programmeId, out options))
                                {
                                    programme["voteOptions"] = options;
                                }
                                else
                                {
                                    programme["voteOptions"] = new List<object>();
                                }

                                Dictionary<String, object> placeholder = null;
                                if (OptionPlaceHolderSet.TryGetValue(programmeId, out placeholder))
                                {
                                    placeholder["type"] = reader["selectOptionsDisplayType"];

                                    programme["votePlaceholder"] = placeholder;

                                }
                                else
                                {
                                    placeholder = new Dictionary<string, object>();
                                    options = new List<object>();
                                    placeholder.Add("type", "");
                                    placeholder.Add("sequence", options);
                                    programme["votePlaceholder"] = placeholder;
                                }
                                programmes.Add(programme);
                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                    strb.AppendLine("Got programmes: " + watch.ElapsedMilliseconds);
                }
                #endregion

                Common.programmesList = new List<Dictionary<string, object>>();
                Common.programmesList = programmes;
                return strb.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

         /// <summary>
        /// 根据programmeId生成静态的programme json文件，
        /// </summary>
        /// <returns></returns>
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                   UriTemplate = "/generateProgrammesStaticFile")]
        private string GenerateProgrammesStaticFile()
        {
            try
            {
                if (bool.Parse(Common.EnableGenerateStaticFile))
                {
                    StringBuilder strb = new StringBuilder();
                    List<Dictionary<string, object>> programmeList = Common.programmesList;
                    if (programmeList.Count > 0)
                    {
                        Dictionary<string, object> rootProgramme = ProgrammeHelper.FilterById(programmeList, 0, false, null);
                        Common.rootProgramme = new Dictionary<string, object>();
                        Common.rootProgramme = rootProgramme;
                        strb.AppendLine(Common.UploadFile(rootProgramme, Common.StorageConnectionString,
                            Common.JSON_v2_3_Folder, "programme_0.json", "application/json", true));

                        //await GoogleCloud.AddRootProgrammeListData(rootProgramme);
                        List<String> uploadJsonNameList = new List<string>();
                        Common.programmeIdList = new List<Dictionary<string, object>>();
                        foreach (var item in programmeList)
                        {
                            long pId = Convert.ToInt64(item["programmeId"]);
                            int isAuthPassword = Convert.ToInt32(item["isAuthPassword"]);
                            String basicAuthPassword = Convert.ToString(item["basicAuthPassword"]);
                            String jsonName = Common.GetProgrammeName(isAuthPassword, pId.ToString(), basicAuthPassword);

                            Dictionary<string, object> programme = ProgrammeHelper.FilterById(programmeList, pId, false, null);
                            programme.Remove("isAuthPassword");
                            programme.Remove("basicAuthPassword");

                            int listingBannerImageHeight = Common.CheckIntNull(item["listingBannerImageHeight"]);
                            int listingBannerImageWeight = Common.CheckIntNull(item["listingBannerImageWeight"]);
                            int listingBackgroundImageHeight = Common.CheckIntNull(item["listingBackgroundImageHeight"]);
                            int listingBackgroundImageWeight = Common.CheckIntNull(item["listingBackgroundImageWeight"]);
                            int identityBannerImageHeight = Common.CheckIntNull(item["identityBannerImageHeight"]);
                            int identityBannerImageWeight = Common.CheckIntNull(item["identityBannerImageWeight"]);
                            int backgroundFooterImageHeight = Common.CheckIntNull(item["backgroundFooterImageHeight"]);
                            int backgroundFooterImageWeight = Common.CheckIntNull(item["backgroundFooterImageWeight"]);
                            int backgroundImageHeight = Common.CheckIntNull(item["backgroundImageHeight"]);
                            int backgroundImageWeight = Common.CheckIntNull(item["backgroundImageWeight"]);

                            programme.Add("listingBannerImageSize", Common.ConvertToSize(listingBannerImageWeight, listingBannerImageHeight));
                            programme.Add("listingBackgroundImageSize", Common.ConvertToSize(listingBackgroundImageWeight, listingBackgroundImageHeight));
                            programme.Add("identityBannerImageSize", Common.ConvertToSize(identityBannerImageWeight, identityBannerImageHeight));
                            programme.Add("backgroundFooterImageSize", Common.ConvertToSize(backgroundFooterImageWeight, backgroundFooterImageHeight));
                            programme.Add("backgroundImageSize", Common.ConvertToSize(backgroundImageWeight, backgroundImageHeight));

                            programme.Remove("listingBannerImageHeight");
                            programme.Remove("listingBannerImageWeight");
                            programme.Remove("listingBackgroundImageHeight");
                            programme.Remove("listingBackgroundImageWeight");
                            programme.Remove("identityBannerImageHeight");
                            programme.Remove("identityBannerImageWeight");
                            programme.Remove("backgroundFooterImageHeight");
                            programme.Remove("backgroundFooterImageWeight");
                            programme.Remove("backgroundImageHeight");
                            programme.Remove("backgroundImageWeight");

                            Common.programmeIdList.Add(programme);
                            strb.AppendLine(Common.UploadFile(programme, Common.StorageConnectionString,
                                Common.JSON_v2_3_Folder, jsonName, "application/json", true));

                            uploadJsonNameList.Add(jsonName);
                        }

                        #region 过期文件处理
                        if (Common.EnableUploadStorage.ToLower().Trim() == "true")
                        {
                            //获取Bolb上所有的文件
                            List<String> cloudJsonNameList = Util.DownloadAllFile(Common.StorageConnectionString, Common.JSON_v2_3_Folder);
                            //删除过期文件
                            if (cloudJsonNameList.Count > 0)
                            {
                                foreach (var item in cloudJsonNameList)
                                {
                                    if (!uploadJsonNameList.Contains(item))
                                    {
                                        if (item.Equals("systemConfig.json", StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            continue;
                                        }
                                        else if (item.Equals("programme_0.json", StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            continue;
                                        }
                                        else if (item.Contains("_flag.json"))
                                        {
                                            continue;
                                        }
                                        else
                                        {
                                            Util.DeleteFile(Common.StorageConnectionString, Common.JSON_v2_3_Folder, item);
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                    }
                    return strb.ToString();
                }
                else
                {
                    return "EnableGenerateStaticFile=false";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        /// <summary>
        /// 存放数据存放的Google cloud
        /// </summary>
        private async void UploudGoogleCloud()
        {
            await GoogleCloud.AddSystemConfigData(Common.systemConfigList);
            await GoogleCloud.AddRootProgrammeListData(Common.rootProgramme);
            await GoogleCloud.deleteProgrammeListData(Common.programmesId_name);
            Common.programmesId_name = new List<string>();
            if (Common.programmeIdList.Count > 0)
            {
                foreach (var item in Common.programmeIdList)
                {
                    long pId = Convert.ToInt64(item["programmeId"]);
                    Common.programmesId_name.Add("" + pId + "");
                    await GoogleCloud.AddProgrammeListData(item, "" + pId + "");
                }
            }
        }

        /// <summary>
        /// 获取configinfo
        /// </summary>
        /// <returns></returns>
        public List<ConfigInfo> GetConfigList()
        {
            List<ConfigInfo> configInfo=new List<ConfigInfo>();
            try
            {
                if (Common.configInfoList.Count > 0)
                {
                    configInfo = Common.configInfoList;
                }
                else
                {
                    Common.SetConfigInfo();
                    GetConfigList();
                }
            }
            catch (Exception ex)
            {

            }
            return configInfo;
        }
      
    }
}