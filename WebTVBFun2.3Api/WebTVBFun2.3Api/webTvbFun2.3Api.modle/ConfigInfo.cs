﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebTVBFun2._3Api.webTvbFun2._3Api.modle
{
    public class ConfigInfo
    {
        public string Name { get; set; }
        public string UrlPrefix { get; set; }
        public string JSON_v2_3_Folder { get; set; }
        public string ImageUrlPrefix { get; set; }
        public bool EnableUploadStorage { get; set; }
        public bool EnableUploadFTP { get; set; }
        public bool EnableUploadBak { get; set; }


    }
}