﻿using Microsoft.Owin;
using Owin;
using WebTVBFun2._3Api.tvbFunApi;
using WebTVBFun2._3Api.webTvbFun2._3Api.common;

[assembly: OwinStartupAttribute(typeof(WebTVBFun2._3Api.Startup))]
namespace WebTVBFun2._3Api
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            Common.SetConfigInfo();
        }
    }
}
