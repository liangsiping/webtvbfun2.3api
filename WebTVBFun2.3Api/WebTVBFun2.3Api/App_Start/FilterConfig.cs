﻿using System.Web;
using System.Web.Mvc;

namespace WebTVBFun2._3Api
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
