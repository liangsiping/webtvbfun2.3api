﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebTVBFun2._3Api.webTvbFun2._3Api.dao
{
    public class DBDao
    {
        protected string sqlconnection;

        /// <summary>
        /// 获取链接字符串
        /// </summary>
        public DBDao()
        {
            sqlconnection = ConfigurationManager.ConnectionStrings["CMSDBConnection"].ConnectionString;
        }
        /// <summary>
        /// 打开数据库链接
        /// </summary>
        /// <returns></returns>

        public SqlConnection OpenConnection()
        {
            SqlConnection connection = new SqlConnection(sqlconnection);
            try
            {
                connection.Open();
                return connection;
            }
            catch (Exception)
            {
                return null;
            }
        }

    }
}