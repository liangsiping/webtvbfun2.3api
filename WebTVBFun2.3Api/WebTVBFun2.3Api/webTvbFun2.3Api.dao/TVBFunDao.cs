﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Dapper;
using WebTVBFun2._3Api.webTvbFun2._3Api.modle;

namespace WebTVBFun2._3Api.webTvbFun2._3Api.dao
{
    public class TVBFunDao:DBDao
    {
        /// <summary>
        /// 获取system config 列表
        /// </summary>
        /// <returns></returns>
        public List<ConfigInfo> GetConfigInfoList(string name)
        {
            List<ConfigInfo> list = new List<ConfigInfo>();
            try
            {
                using (IDbConnection conn = OpenConnection())
                {
                    string query = String.Format(@"select Name,UrlPrefix,JSON_v2_3_Folder,ImageUrlPrefix,EnableUploadStorage,EnableUploadBak,EnableUploadFTP from ConfigInfo where Name='" + name + "'");
                    list = conn.Query<ConfigInfo>(query, null).AsList();
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException("DB Error!");
            }
            return list;
        }
     
    }
}