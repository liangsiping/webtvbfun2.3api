﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

namespace WebTVBFun2._3Api.webTvbFun2._3Api.common
{
    public class ProgrammeHelper
    {
        /// <summary>
        /// programme 数据处理
        /// </summary>
        /// <param name="programmeList"></param>
        /// <param name="programmeId"></param>
        /// <param name="filterGroups"></param>
        /// <param name="groups"></param>
        /// <returns></returns>
        public static Dictionary<string, object> FilterById(List<Dictionary<string, object>> programmeList, long programmeId, bool filterGroups, string groups)
        {
            string ImageUrlPrefix = Common.ImageUrlPrefix;
            //JObject jObject = (JObject)cache.Get("programmeList");
            Dictionary<string, object> parentProgram = null;
            List<Dictionary<string, object>> filterList = new List<Dictionary<string, object>>();
            int displayGroup = 0;
            int gameMode = 0;
            if (programmeList.Count>0)
            {
                List<String> _groups = new List<String>();
                if (!String.IsNullOrEmpty(groups))
                {
                    _groups.AddRange(groups.Split(','));
                }
                _groups.Add("");

                foreach (var item in programmeList)
                {
                    String group = Convert.ToString(item["memberGroupId"]);
                    long pId = Convert.ToInt64(item["programmeId"]);
                    long parentid = Convert.ToInt64(item["parentProgrammeId"]);
                    displayGroup = Convert.ToInt32(item["displayGroup"]);
                    gameMode = Convert.ToInt32(item["gameMode"]);

                    if (parentid == programmeId)
                    {
                        if (filterGroups == false || _groups.Contains(group))
                        {
                            try
                            {
                                int listingBannerImageHeight = CheckIntNull(item["listingBannerImageHeight"]);
                                int listingBannerImageWeight = CheckIntNull(item["listingBannerImageWeight"]);
                                int listingBackgroundImageHeight = CheckIntNull(item["listingBackgroundImageHeight"]);
                                int listingBackgroundImageWeight = CheckIntNull(item["listingBackgroundImageWeight"]);
                                int identityBannerImageHeight = CheckIntNull(item["identityBannerImageHeight"]);
                                int identityBannerImageWeight = CheckIntNull(item["identityBannerImageWeight"]);
                                int backgroundFooterImageHeight = CheckIntNull(item["backgroundFooterImageHeight"]);
                                int backgroundFooterImageWeight = CheckIntNull(item["backgroundFooterImageWeight"]);
                                int backgroundImageHeight = CheckIntNull(item["backgroundImageHeight"]);
                                int backgroundImageWeight = CheckIntNull(item["backgroundImageWeight"]);

                                Dictionary<string, object> programme = new Dictionary<string, object>() { 
                                {"programmeId",item["programmeId"]},
                                {"programmeCode",item["programmeCode"].ToString()},
                                {"title",item["title"].ToString()},
                                {"showTime",item["showTime"].ToString()},
                                {"displayGroup",int.Parse(item["displayGroup"].ToString())},
                                {"gameMode",int.Parse(item["gameMode"].ToString())},
                                {"channel",item["channel"]},
                                //{"parentProgrammeId",item.Value["parentProgrammeId"]},
                                {"memberGroupId",item["memberGroupId"].ToString()},
                                {"hasSubProgrammes",int.Parse(item["isAuthPassword"].ToString())},
                                {"listingBannerImageUrl",item["listingBannerImageUrl"].ToString()},
                                {"listingBackgroundImageUrl",item["listingBackgroundImageUrl"].ToString()},
                                {"identityBannerImageUrl",item["identityBannerImageUrl"].ToString()},

                                {"submitBtnImage",item["submitBtnImage"].ToString()},
                                {"resetBtnImage",item["resetBtnImage"].ToString()},
                                {"continueGameBtnImage",item["continueGameBtnImage"].ToString()},
                                {"scoreDisplayBackgroundImage",item["scoreDisplayBackgroundImage"].ToString()},
                                {"backgroundImage",item["backgroundImage"].ToString()},
                                {"backgroundRepeatYImage",item["backgroundRepeatYImage"].ToString()},
                                {"backgroundFooterImage",item["backgroundFooterImage"].ToString()},

                                {"thumbnailUrl",item["thumbnailUrl"].ToString()},
                                {"backgroundColorCode",item["backgroundColorCode"].ToString()},
                                {"presentationType",item["presentationType"].ToString()},
                                {"gamePageActiveFlag",int.Parse(item["gamePageActiveFlag"].ToString())},

                                {"enableBorder",int.Parse(item["enableBorder"].ToString())},
                                {"clickable",int.Parse(item["clickable"].ToString())},
                                
                                {"hasRealTimeGameActiveFlag",item["hasRealTimeGameActiveFlag"]},
                                {"listingBackgroudColorCode",item["listingBackgroudColorCode"].ToString()},
                                {"activeTimeInterval",int.Parse(item["activeTimeInterval"].ToString())},
                                {"isAuthPassword",int.Parse(item["isAuthPassword"].ToString())},
                                
                                {"listingBannerImageSize",ConvertSize(listingBannerImageWeight,listingBannerImageHeight)},
                                {"listingBackgroundImageSize",ConvertSize(listingBackgroundImageWeight,listingBackgroundImageHeight)},
                                {"identityBannerImageSize",ConvertSize(identityBannerImageWeight,identityBannerImageHeight)},
                                {"backgroundFooterImageSize",ConvertSize(backgroundFooterImageWeight,backgroundFooterImageHeight)},
                                {"backgroundImageSize",ConvertSize(backgroundImageWeight,backgroundImageHeight)},
                             };

                                filterList.Add(programme);
                            }
                            catch (Exception ex)
                            {
                                
                            }
                        }
                    }
                    else if (pId == programmeId)
                    {
                        parentProgram = item;
                    }
                }
            }
            if (parentProgram != null)
            {
                parentProgram["subProgrammes"] = filterList;
                return parentProgram;
            }
            else if (programmeId > 0)
            {
                //programmeId not found;

                Dictionary<string, object> defaultParentProgram = new Dictionary<string, object>() { 
                    {"programmeId",-1},
                    {"programmeCode",""},
                    {"title","Programme Not Found"},
                    {"hasSubProgrammes",0},
                    {"parentProgrammeId",-1},
                    {"displayGroup",0},
                    {"gameMode",0}
                 };
                return defaultParentProgram;
            }
            else
            {
                Object title = "";
                try
                {
                    String json = TVBCOM.SDK.Net.HttpLoader.Get(Common.LinkOfProgramme_0_Title);
                    title = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(json).Values.First();
                }
                catch (Exception)
                {
                    title = "Big Big Fun";
                    //throw;
                }
                //root programme list; 
                //层级 programme_0
                Dictionary<string, object> defaultParentProgram = new Dictionary<string, object>() { 
                    {"programmeId",0},
                    {"programmeCode",""},
                    {"title",title},
                    {"hasSubProgrammes",1},
                    {"parentProgrammeId",0},
                    {"subProgrammes",filterList},
                    {"displayGroup",displayGroup},
                    {"gameMode",gameMode}
                 };
                return defaultParentProgram;
            }

        }
        /// <summary>
        /// 数据处理
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="urlPrefix"></param>
        /// <param name="channels"></param>
        /// <returns></returns>
        public static Dictionary<string, object> parseDictionaryFromReader(IDataReader reader, String urlPrefix, Dictionary<string, string> channels)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            for (int i = 0; i < reader.FieldCount; i++)
            {
                object val = reader[i];
                String key = reader.GetName(i);
                if ((val != null) && (key.ToLower().Contains("url") || key.ToLower().Contains("image")))
                {
                    if (key.ToLower().Contains("weight") == false && key.ToLower().Contains("height") == false)
                    {

                        String url = Convert.ToString(val);
                        if (url.Trim().Length > 0 && !url.ToLower().StartsWith("http"))
                        {
                            url = Path.Combine(urlPrefix, url);
                            val = url;
                        }
                        else if (String.IsNullOrEmpty(url))
                        {
                            val = "";
                        }
                    }
                }
                else if (val != null && key.Equals("channel", StringComparison.OrdinalIgnoreCase))
                {
                    List<String> channelNumList = new List<String>();
                    String[] _channels = val.ToString().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (String channel in _channels)
                    {
                        String channelNum = null;
                        if (channels.TryGetValue(channel.Trim(), out channelNum))
                        {
                            channelNumList.Add(channelNum);
                        }
                    }
                    val = channelNumList;
                }
                else if (val != null && key.Equals("handleVotingErrorCodes", StringComparison.OrdinalIgnoreCase))
                {
                    List<int> errorCodeList = new List<int>();
                    String[] errorCodeArray = val.ToString().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (String errorCode in errorCodeArray)
                    {
                        if (!String.IsNullOrWhiteSpace(errorCode))
                        {
                            errorCodeList.Add(Convert.ToInt32(errorCode.Trim()));
                        }
                    }
                    val = errorCodeList;
                }
                else if (key.Equals("programmeCode", StringComparison.OrdinalIgnoreCase))
                {
                    val = val != null && Convert.ToString(val).Trim().Length > 0 ? val : "general";
                }
                else if (val != null
                    && (
                    key.Equals("serverLinkVoting", StringComparison.OrdinalIgnoreCase)
                    || key.Equals("serverLinksForBuddyPointBalance", StringComparison.OrdinalIgnoreCase)
                    || key.Equals("serverLinksForBuddyPointHistory", StringComparison.OrdinalIgnoreCase)
                    || key.Equals("resultPageMessages", StringComparison.OrdinalIgnoreCase)
                    || key.Equals("resultPageMessagesForWrongAnswer", StringComparison.OrdinalIgnoreCase)
                    )
                    )
                {
                    List<String> serverLinkVotingList = new List<String>();
                    String[] serverLinkVotingArray = val.ToString().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (String serverLinkVoting in serverLinkVotingArray)
                    {
                        if (!String.IsNullOrWhiteSpace(serverLinkVoting))
                        {
                            serverLinkVotingList.Add(serverLinkVoting.Trim());
                        }
                    }
                    val = serverLinkVotingList;
                }

                else if (key.Equals("enableBorder", StringComparison.OrdinalIgnoreCase) || key.Equals("clickable", StringComparison.OrdinalIgnoreCase))
                {
                }
                else if (key.Equals("selectOptionsDisplayType", StringComparison.OrdinalIgnoreCase))
                {
                    continue;
                }
                else if (
                    key.Equals("enableBorder", StringComparison.OrdinalIgnoreCase)
                    || key.Equals("clickable", StringComparison.OrdinalIgnoreCase)
                    || key.Equals("shouldDisplayScore", StringComparison.OrdinalIgnoreCase)
                    || key.Equals("enableCache", StringComparison.OrdinalIgnoreCase)
                    || key.Equals("enableCookies", StringComparison.OrdinalIgnoreCase)
                    || key.Equals("autoRefreshAfterSeconds", StringComparison.OrdinalIgnoreCase)
                    || key.Equals("IsAuthPassword", StringComparison.OrdinalIgnoreCase)
                    )
                {
                    val = Common.CheckIntNull(val);
                }
                if (val != null && val.ToString().ToLower().StartsWith("http"))
                {
                    if ((!val.ToString().ToLower().Contains(".json") && !val.ToString().ToLower().Contains(".html")))
                    {
                        val = val.ToString().Replace(urlPrefix, Common.ImageUrlPrefix);
                    }
                }
                else if (val == null)
                {
                    val = Common.CheckStringNull(val);
                }
                if ((val != null) && (key.ToLower().Contains("date")))
                {
                    val = val.ToString().Replace("T", "");
                }

                object obj;
                if (!dict.TryGetValue(key, out obj))
                {
                    if (val != null && val.ToString() != "")
                    {
                        dict.Add(key, val);
                    }
                    else
                    {
                        dict.Add(key, val.ToString());
                    }
                }
            }

            return dict;
        }

        public static object ConvertSize(int width, int height)
        {
            return new { width = width, height = height };
        }

        private static int CheckIntNull(object obj)
        {
            int result = 0;
            int.TryParse(obj.ToString(), out result);
            return result; ;
        }
    }
}