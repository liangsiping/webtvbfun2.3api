﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using TVBCOM.SDK.Net;

namespace WebTVBFun2._3Api.webTvbFun2._3Api.common
{
    public class SystemConfigV23 : Dictionary<String, Dictionary<String, object>>
    {

    }
    public class VersionConfig
    {
        [JsonProperty("platform")]
        public String Platform { get; set; }
        [JsonProperty("minVersion")]
        public String MinVersion { get; set; }
        [JsonProperty("updateUrlZhTw")]
        public String UpdateUrlZhTw { get; set; }
        [JsonProperty("updateUrlEnUs")]
        public String UpdateUrlEnUs { get; set; }
        [JsonProperty("updateMsgZhtw")]
        public String UpdateMsgZhtw { get; set; }
        [JsonProperty("updateMsgEnUs")]
        public String UpdateMsgEnUs { get; set; }
        [JsonProperty("lastUpdateDate")]
        public long LastUpdateDate { get; set; }
        [JsonProperty("updatedBy")]
        public String UpdatedBy { get; set; }
    }

    public class SystemConfigHelper
    {

        public SystemConfigV23 ParseSystemConfig(String configFileUrl)
        {
            String json = HttpLoader.Get(configFileUrl);
           
            JsonSerializer serializer = new JsonSerializer();
            SystemConfigV23 jObject = JsonConvert.DeserializeObject<SystemConfigV23>(json);
            string channels_zh = jObject["zh"]["channels"].ToString();
            List<Dictionary<string, string>> channels_zh_list = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(channels_zh);

            string serverLinksOfProgrammeInfo_zh = jObject["zh"]["serverLinksOfProgrammeInfo"].ToString();
            List<string> serverLinksOfProgrammeInfo_zh_list = JsonConvert.DeserializeObject<List<string>>(serverLinksOfProgrammeInfo_zh);

            string channels_en = jObject["en"]["channels"].ToString();
            List<Dictionary<string, string>> channels_en_list = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(channels_en);

            string serverLinksOfProgrammeInfo_en = jObject["en"]["serverLinksOfProgrammeInfo"].ToString();
            List<string> serverLinksOfProgrammeInfo_en_list = JsonConvert.DeserializeObject<List<string>>(serverLinksOfProgrammeInfo_en);
            
           SystemConfigV23 newjObject = new SystemConfigV23() { 
                {
                    "zh", new Dictionary<String, Object>(){
                        {String.Format("quitAppMsgURL"), jObject["zh"]["quitAppMsgURL"]},
                        {String.Format("invalidLogonStatusPromptMsg"), jObject["zh"]["invalidLogonStatusPromptMsg"]},
                        {String.Format("errorMsgLoginFailure"), jObject["zh"]["errorMsgLoginFailure"]},
                        {String.Format("memberRegRemindMessage_UpgradeUser"), jObject["zh"]["memberRegRemindMessage_UpgradeUser"]},
                        {String.Format("correctAnsPeriodFrom_Buffer_ForInstantWin"), jObject["zh"]["correctAnsPeriodFrom_Buffer_ForInstantWin"]},
                        {String.Format("serverLinkRewardItem"), jObject["zh"]["serverLinkRewardItem"]},
                        {String.Format("casualUserRegRemindMessage_FirstRegister"), jObject["zh"]["casualUserRegRemindMessage_FirstRegister"]},
                        {String.Format("emergencyEmailList"), jObject["zh"]["emergencyEmailList"]},
                        {String.Format("serverLinkCheckAppVersion"), jObject["zh"]["serverLinkCheckAppVersion"]},
                        {String.Format("programmeInfoReloadMin"), jObject["zh"]["programmeInfoReloadMin"]},
                        {String.Format("serverLinksOfProgrammeInfo"), serverLinksOfProgrammeInfo_zh_list},
                        {String.Format("quitAppFlag"), jObject["zh"]["quitAppFlag"]},
                        {String.Format("errorMsgGeneral"), jObject["zh"]["errorMsgGeneral"]},
                        {String.Format("errorMsgInactiveAccount"), jObject["zh"]["errorMsgInactiveAccount"]},
                        {String.Format("afterVoteSoundEffect"), jObject["zh"]["afterVoteSoundEffect"]},
                        {String.Format("serverLinkMyItem"), jObject["zh"]["serverLinkMyItem"]},
                        {String.Format("casualUserRegRemarkContent"), jObject["zh"]["casualUserRegRemarkContent"]},
                        {String.Format("basicAuthGatedMessage"), jObject["zh"]["basicAuthGatedMessage"]},
                        {String.Format("linkPushServerIOS"), jObject["zh"]["linkPushServerIOS"]},
                        {String.Format("bannerUpdateTime"), jObject["zh"]["bannerUpdateTime"]},
                        {String.Format("regRemarkContent"), jObject["zh"]["regRemarkContent"]},
                        {String.Format("serverLinkBannerJson"), jObject["zh"]["serverLinkBannerJson"]},
                        {String.Format("subAccountLimit"), jObject["zh"]["subAccountLimit"]},
                        {String.Format("bannerAdsOnHoldSec"), jObject["zh"]["bannerAdsOnHoldSec"]},
                        {String.Format("linkPushServerAndroid"), jObject["zh"]["linkPushServerAndroid"]},
                        {String.Format("channels"), channels_zh_list},
                        {String.Format("linkPrivacy"), jObject["zh"]["linkPrivacy"]},
                        {String.Format("linkStartUpTandC"), jObject["zh"]["linkStartUpTandC"]},
                        {String.Format("modifiedDate"), jObject["zh"]["modifiedDate"]},
                        {String.Format("additionalBannerOnHold"), jObject["zh"]["additionalBannerOnHold"]},
                        {String.Format("quitAppMtimeoutOfProgrammeInfoInSecsgURL"), jObject["zh"]["timeoutOfProgrammeInfoInSec"]},
                        {String.Format("enableAnalytics"), jObject["zh"]["enableAnalytics"]},
                        {String.Format("defaultBannerFullPage"), jObject["zh"]["defaultBannerFullPage"]},
                        {String.Format("casualUserRegRemindMessage_RegisterAgain"), jObject["zh"]["casualUserRegRemindMessage_RegisterAgain"]},
                        {String.Format("basicAuthFailedMessage"), jObject["zh"]["basicAuthFailedMessage"]},
                        {String.Format("linkMemberRegTandC"), jObject["zh"]["linkMemberRegTandC"]},
                        {String.Format("linkPushServerWP"), jObject["zh"]["linkPushServerWP"]},
                        {String.Format("serverLinkRedemption"), jObject["zh"]["serverLinkRedemption"]},
                        {String.Format("errorMsgDuplicateVote"), jObject["zh"]["errorMsgDuplicateVote"]},
                        {String.Format("correctAnsPeriodTo_Buffer_ForInstantWin"), jObject["zh"]["correctAnsPeriodTo_Buffer_ForInstantWin"]},
                        {String.Format("serverLinkGaming"), jObject["zh"]["serverLinkGaming"]},
                        {String.Format("defaultClickThroughLink"), jObject["zh"]["defaultClickThroughLink"]},
                        {String.Format("defaultBannerBottom"), jObject["zh"]["defaultBannerBottom"]},
                        {String.Format("modifiedBy"), jObject["zh"]["modifiedBy"]},
                        {String.Format("verificationCodeLength"), jObject["zh"]["verificationCodeLength"]},
                        {String.Format("linkInfo"), jObject["zh"]["linkInfo"]},
                        {String.Format("linkDisclaimer"), jObject["zh"]["linkDisclaimer"]},
                        {String.Format("linkPrizeCollectionNotice"), jObject["zh"]["linkPrizeCollectionNotice"]},
                        {String.Format("quitAppMsg"), jObject["zh"]["quitAppMsg"]},
                        {String.Format("serverLinkMember"), jObject["zh"]["serverLinkMember"]},
                        {String.Format("linkAdFunConfig"), jObject["zh"]["linkAdFunConfig"]},
                        {String.Format("linkTandC"), jObject["zh"]["linkTandC"]},
                        {String.Format("membershipVer"), jObject["zh"]["membershipVer"]},
                        {String.Format("memberRegRemindMessage_NewUser"), jObject["zh"]["memberRegRemindMessage_NewUser"]},
                        {String.Format("linkHelp"), jObject["zh"]["linkHelp"]},
                        {String.Format("serverLinkNotificationJson"), jObject["zh"]["serverLinkNotificationJson"]},
                        {String.Format("serverLinkVoting"), jObject["zh"]["serverLinkVoting"]},
                        {String.Format("serverLinkProgrammeInfo"), jObject["zh"]["serverLinkProgrammeInfo"]},
                        {String.Format("serverLinkRewardJson"), jObject["zh"]["serverLinkRewardJson"]},
                        {String.Format("cloudRecognitionOn"), jObject["zh"]["cloudRecognitionOn"]},
                        {String.Format("bannerDisplayMode"), jObject["zh"]["bannerDisplayMode"]},
                        {String.Format("linkLuckyDrawConfig"), jObject["zh"]["linkLuckyDrawConfig"]},
                        {String.Format("newRegistrationPopupMessage"), jObject["zh"]["newRegistrationPopupMessage"]}
                    }
                },
                {
                    "en", new Dictionary<String, Object>(){
                        {String.Format("quitAppMsgURL"), jObject["en"]["quitAppMsgURL"]},
                        {String.Format("invalidLogonStatusPromptMsg"), jObject["en"]["invalidLogonStatusPromptMsg"]},
                        {String.Format("errorMsgLoginFailure"), jObject["en"]["errorMsgLoginFailure"]},
                        {String.Format("memberRegRemindMessage_UpgradeUser"), jObject["en"]["memberRegRemindMessage_UpgradeUser"]},
                        {String.Format("correctAnsPeriodFrom_Buffer_ForInstantWin"), jObject["en"]["correctAnsPeriodFrom_Buffer_ForInstantWin"]},
                        {String.Format("serverLinkRewardItem"), jObject["en"]["serverLinkRewardItem"]},
                        {String.Format("casualUserRegRemindMessage_FirstRegister"), jObject["en"]["casualUserRegRemindMessage_FirstRegister"]},
                        {String.Format("emergencyEmailList"), jObject["en"]["emergencyEmailList"]},
                        {String.Format("serverLinkCheckAppVersion"), jObject["en"]["serverLinkCheckAppVersion"]},
                        {String.Format("programmeInfoReloadMin"), jObject["en"]["programmeInfoReloadMin"]},
                        {String.Format("serverLinksOfProgrammeInfo"), serverLinksOfProgrammeInfo_en_list},
                        {String.Format("quitAppFlag"), jObject["en"]["quitAppFlag"]},
                        {String.Format("errorMsgGeneral"), jObject["en"]["errorMsgGeneral"]},
                        {String.Format("errorMsgInactiveAccount"), jObject["en"]["errorMsgInactiveAccount"]},
                        {String.Format("afterVoteSoundEffect"), jObject["en"]["afterVoteSoundEffect"]},
                        {String.Format("serverLinkMyItem"), jObject["en"]["serverLinkMyItem"]},
                        {String.Format("casualUserRegRemarkContent"), jObject["en"]["casualUserRegRemarkContent"]},
                        {String.Format("basicAuthGatedMessage"), jObject["en"]["basicAuthGatedMessage"]},
                        {String.Format("linkPushServerIOS"), jObject["en"]["linkPushServerIOS"]},
                        {String.Format("bannerUpdateTime"), jObject["en"]["bannerUpdateTime"]},
                        {String.Format("regRemarkContent"), jObject["en"]["regRemarkContent"]},
                        {String.Format("serverLinkBannerJson"), jObject["en"]["serverLinkBannerJson"]},
                        {String.Format("subAccountLimit"), jObject["en"]["subAccountLimit"]},
                        {String.Format("bannerAdsOnHoldSec"), jObject["en"]["bannerAdsOnHoldSec"]},
                        {String.Format("linkPushServerAndroid"), jObject["en"]["linkPushServerAndroid"]},
                        {String.Format("channels"), channels_en_list},
                        {String.Format("linkPrivacy"), jObject["en"]["linkPrivacy"]},
                        {String.Format("linkStartUpTandC"), jObject["en"]["linkStartUpTandC"]},
                        {String.Format("modifiedDate"), jObject["en"]["modifiedDate"]},
                        {String.Format("additionalBannerOnHold"), jObject["en"]["additionalBannerOnHold"]},
                        {String.Format("quitAppMtimeoutOfProgrammeInfoInSecsgURL"), jObject["en"]["timeoutOfProgrammeInfoInSec"]},
                        {String.Format("enableAnalytics"), jObject["en"]["enableAnalytics"]},
                        {String.Format("defaultBannerFullPage"), jObject["en"]["defaultBannerFullPage"]},
                        {String.Format("casualUserRegRemindMessage_RegisterAgain"), jObject["en"]["casualUserRegRemindMessage_RegisterAgain"]},
                        {String.Format("basicAuthFailedMessage"), jObject["en"]["basicAuthFailedMessage"]},
                        {String.Format("linkMemberRegTandC"), jObject["en"]["linkMemberRegTandC"]},
                        {String.Format("linkPushServerWP"), jObject["en"]["linkPushServerWP"]},
                        {String.Format("serverLinkRedemption"), jObject["en"]["serverLinkRedemption"]},
                        {String.Format("errorMsgDuplicateVote"), jObject["en"]["errorMsgDuplicateVote"]},
                        {String.Format("correctAnsPeriodTo_Buffer_ForInstantWin"), jObject["en"]["correctAnsPeriodTo_Buffer_ForInstantWin"]},
                        {String.Format("serverLinkGaming"), jObject["en"]["serverLinkGaming"]},
                        {String.Format("defaultClickThroughLink"), jObject["en"]["defaultClickThroughLink"]},
                        {String.Format("defaultBannerBottom"), jObject["en"]["defaultBannerBottom"]},
                        {String.Format("modifiedBy"), jObject["en"]["modifiedBy"]},
                        {String.Format("verificationCodeLength"), jObject["en"]["verificationCodeLength"]},
                        {String.Format("linkInfo"), jObject["en"]["linkInfo"]},
                        {String.Format("linkDisclaimer"), jObject["en"]["linkDisclaimer"]},
                        {String.Format("linkPrizeCollectionNotice"), jObject["en"]["linkPrizeCollectionNotice"]},
                        {String.Format("quitAppMsg"), jObject["en"]["quitAppMsg"]},
                        {String.Format("serverLinkMember"), jObject["en"]["serverLinkMember"]},
                        {String.Format("linkAdFunConfig"), jObject["en"]["linkAdFunConfig"]},
                        {String.Format("linkTandC"), jObject["en"]["linkTandC"]},
                        {String.Format("membershipVer"), jObject["en"]["membershipVer"]},
                        {String.Format("memberRegRemindMessage_NewUser"), jObject["en"]["memberRegRemindMessage_NewUser"]},
                        {String.Format("linkHelp"), jObject["en"]["linkHelp"]},
                        {String.Format("serverLinkNotificationJson"), jObject["en"]["serverLinkNotificationJson"]},
                        {String.Format("serverLinkVoting"), jObject["en"]["serverLinkVoting"]},
                        {String.Format("serverLinkProgrammeInfo"), jObject["en"]["serverLinkProgrammeInfo"]},
                        {String.Format("serverLinkRewardJson"), jObject["en"]["serverLinkRewardJson"]},
                        {String.Format("cloudRecognitionOn"), jObject["en"]["cloudRecognitionOn"]},
                        {String.Format("bannerDisplayMode"), jObject["en"]["bannerDisplayMode"]},
                        {String.Format("linkLuckyDrawConfig"), jObject["en"]["linkLuckyDrawConfig"]},
                        {String.Format("newRegistrationPopupMessage"), jObject["en"]["newRegistrationPopupMessage"]}
                    }
                }
            };
           return newjObject;

        }

        public SystemConfigV23 ParseVersionConfig(String configFileUrl, String platform)
        {
            String json = HttpLoader.Get(configFileUrl);
            JsonSerializer serializer = new JsonSerializer();
            VersionConfig versionConfigObject = JsonConvert.DeserializeObject<VersionConfig>(json);
            SystemConfigV23 jObject = new SystemConfigV23() { 
                {
                    "zh", new Dictionary<String, Object>(){
                        {String.Format("min{0}Version", platform), versionConfigObject.MinVersion},
                        {String.Format("{0}VersionUpdateMessage", platform.ToLower()), versionConfigObject.UpdateMsgZhtw},
                        {String.Format("{0}VersionUpdateUrl", platform.ToLower()), versionConfigObject.UpdateUrlZhTw}
                    }
                },
                {
                    "en", new Dictionary<String, Object>(){
                        {String.Format("min{0}Version", platform), versionConfigObject.MinVersion},
                        {String.Format("{0}VersionUpdateMessage", platform.ToLower()), versionConfigObject.UpdateMsgEnUs},
                        {String.Format("{0}VersionUpdateUrl", platform.ToLower()), versionConfigObject.UpdateUrlEnUs}
                    }
                }
            };
            return jObject;
        }


        /// <summary>
        /// 组装由CMS生成的Json文件，最终变成App所需要的Json文件
        /// </summary>
        /// <returns>Final json that app needs</returns>

        public SystemConfigV23 getSystemConfigFromStorage()
        {
            //解析CMS的json，包括SystemConfig和3个force upgrade versoin json文件
            SystemConfigV23 systemConfig = ParseSystemConfig(Common.LinkOfSystemConfig);
            SystemConfigV23 iosVersionConfig = ParseVersionConfig(Common.LinkOfForceUpgradeIOS, "IOS");
            SystemConfigV23 androidVersionConfig = ParseVersionConfig(Common.LinkOfForceUpgradeAndroid, "Android");
            SystemConfigV23 wpVersionConfig = ParseVersionConfig(Common.LinkOfForceUpgradeWp, "WP");

            /**
             * 组装CMS的SystemConfig，包括data,force upgrade version
             **/
            // string serverLinksOfProgrammeInfo = systemConfig["zh"]["serverLinksOfProgrammeInfo"].ToString();
            // string serverLinkProgrammeInfo = systemConfig["zh"]["serverLinkProgrammeInfo"].ToString();
            //systemConfig["zh"]["serverLinksOfProgrammeInfo"] = "http://tvbdevestorage.blob.core.windows.net/tvbfundev2/programme_{0}.json";
            // systemConfig["zh"]["serverLinkProgrammeInfo"] = "http://tvbdevestorage.blob.core.windows.net/tvbfundev2/programme_{0}.json";

            systemConfig["en"] = systemConfig["en"].Concat(iosVersionConfig["en"]).ToDictionary(x => x.Key, x => x.Value);
            systemConfig["zh"] = systemConfig["zh"].Concat(iosVersionConfig["zh"]).ToDictionary(x => x.Key, x => x.Value);
            systemConfig["en"] = systemConfig["en"].Concat(androidVersionConfig["en"]).ToDictionary(x => x.Key, x => x.Value);
            systemConfig["zh"] = systemConfig["zh"].Concat(androidVersionConfig["zh"]).ToDictionary(x => x.Key, x => x.Value);
            systemConfig["en"] = systemConfig["en"].Concat(wpVersionConfig["en"]).ToDictionary(x => x.Key, x => x.Value);
            systemConfig["zh"] = systemConfig["zh"].Concat(wpVersionConfig["zh"]).ToDictionary(x => x.Key, x => x.Value);

            //生成最后App所需要的Json
            return systemConfig;
        }
    }

}