﻿using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Microsoft.WindowsAzure.Storage;
using System.Configuration;

namespace WebTVBFun2._3Api.webTvbFun2._3Api.common
{
    public class Util
    {
        private static Dictionary<String, CloudBlockBlob> list = new Dictionary<string, CloudBlockBlob>();
        public static String CDNCacheControlMaxAge = ConfigurationManager.AppSettings["CDNCacheControlMaxAge"];
        public static String UpperFirstChar(String str)
        {
            char[] a = str.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }

        public static Stream CreateJsonResponseStream(Object obj)
        {
            var prop = new HttpResponseMessageProperty();

            MemoryStream ms = new MemoryStream();
            JsonSerializer serializer = new JsonSerializer();
            TextWriter writer = new StreamWriter(ms, Encoding.UTF8);
            serializer.Serialize(writer, obj);
            writer.Flush();
            ms.Seek(0, SeekOrigin.Begin);

            prop.Headers.Set("Content-Type", "application/json; charset=utf-8");
            prop.Headers.Set("Cache-Control", "public, max-age=60");
            prop.Headers.Set("Last-Modified", DateTime.UtcNow.AddSeconds(-1).ToString("R"));
            //prop.Headers.Set("Content-MD5", GetMd5Hash(ms.ToArray()));
            System.ServiceModel.OperationContext.Current.OutgoingMessageProperties.Add(HttpResponseMessageProperty.Name, prop);
            return ms;

            /*********
            return new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(obj)));
            * *******/

        }
        private static CloudBlockBlob GetCloudBlobClient(string connectionString, string containerName, string fileName, string mimeType)
        {
            CloudBlockBlob client;
            String key = connectionString + containerName + fileName + mimeType;

            if (!list.TryGetValue(key, out client))
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);

                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve reference to a previously created container.
                CloudBlobContainer container = blobClient.GetContainerReference(containerName);

                if (container.CreateIfNotExists())
                {
                    var permission = container.GetPermissions();
                    permission.PublicAccess = BlobContainerPublicAccessType.Container;
                    container.SetPermissions(permission);
                }

                Encoding utf8Encoding = new UTF8Encoding(false);
                // Retrieve reference to a blob named "myblob".
                client = container.GetBlockBlobReference(fileName);
                client.Properties.ContentType = mimeType;
                client.Properties.ContentEncoding = utf8Encoding.WebName;
                client.Properties.CacheControl = "public, max-age=" + CDNCacheControlMaxAge;
                list.Add(key, client);
            }

            return client;
        }

        public static string UploadFile(object data, string connectionString, string containerName, string fileName, string mimeType)
        {
            CloudBlockBlob blockBlob = GetCloudBlobClient(connectionString, containerName, fileName, mimeType);

            using (MemoryStream stream = new MemoryStream())
            {
                Encoding utf8Encoding = new UTF8Encoding(false);
                JsonSerializer serializer = new JsonSerializer();
                using (TextWriter writer = new StreamWriter(stream, utf8Encoding))
                {

                    serializer.Serialize(writer, data);
                    writer.Flush();
                    stream.Seek(0, SeekOrigin.Begin);
                    //blockBlob.SetProperties();
                    blockBlob.UploadFromStream(stream);
                }
                blockBlob.SetProperties();
            }

            return blockBlob.Uri.AbsoluteUri;
        }

        public static List<String> DownloadAllFile(string connectionString, string containerName)
        {
            List<String> result = new List<string>();

            // Retrieve storage account from connection string.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
            // Create the blob client. 
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference(containerName);
            // Loop over items within the container and output the length and URI.
            foreach (IListBlobItem item in container.ListBlobs(null, false))
            {
                if (item.GetType() == typeof(CloudBlockBlob))
                {
                    CloudBlockBlob blob = (CloudBlockBlob)item;
                    //Console.WriteLine("Block blob of length {0}: {1}", blob.Properties.Length, blob.Uri);
                    result.Add(blob.Name);
                }
                else if (item.GetType() == typeof(CloudPageBlob))
                {
                    CloudPageBlob pageBlob = (CloudPageBlob)item;
                    //Console.WriteLine("Page blob of length {0}: {1}", pageBlob.Properties.Length, pageBlob.Uri);
                    result.Add(pageBlob.Name);
                }
                else if (item.GetType() == typeof(CloudBlobDirectory))
                {
                    CloudBlobDirectory directory = (CloudBlobDirectory)item;
                    //Console.WriteLine("Directory: {0}", directory.Uri);
                }
            }

            return result;
        }

        public static void DeleteFile(string connectionString, string containerName, string fileName)
        {
            // Retrieve storage account from connection string.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
               connectionString);
            // Create the blob client.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            // Retrieve reference to a previously created container.
            CloudBlobContainer container = blobClient.GetContainerReference(containerName);
            // Retrieve reference to a blob named "myblob.txt".
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);
            // Delete the blob.
            blockBlob.Delete();
        }
 
    }
}