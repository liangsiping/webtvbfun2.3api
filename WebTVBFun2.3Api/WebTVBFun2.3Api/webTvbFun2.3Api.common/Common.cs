﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using WebTVBFun2._3Api.tvbFunApi;
using WebTVBFun2._3Api.webTvbFun2._3Api.dao;
using WebTVBFun2._3Api.webTvbFun2._3Api.modle;

namespace WebTVBFun2._3Api.webTvbFun2._3Api.common
{
    public class Common
    {
        public static String StorageConnectionString = ConfigurationManager.AppSettings["StorageConnectionString"];
        public static String StorageConnectionStringBak = ConfigurationManager.AppSettings["StorageConnectionStringBak"];

        public static String FTPStorageHost = ConfigurationManager.AppSettings["FTPStorageHost"];
        public static String FTPStorageUser = ConfigurationManager.AppSettings["FTPStorageUser"];
        public static String FTPStoragePassword = ConfigurationManager.AppSettings["FTPStoragePassword"];

       

        public static String LinkOfSystemConfig = ConfigurationManager.AppSettings["LinkOfSystemConfig"];
        public static String LinkOfForceUpgradeIOS = ConfigurationManager.AppSettings["LinkOfForceUpgradeIOS"];
        public static String LinkOfForceUpgradeAndroid = ConfigurationManager.AppSettings["LinkOfForceUpgradeAndroid"];
        public static String LinkOfForceUpgradeWp = ConfigurationManager.AppSettings["LinkOfForceUpgradeWp"];

        public static String EncryptProgrammeJsonNameFormat = ConfigurationManager.AppSettings["EncryptProgrammeJsonNameFormat"];

        public static String EnableGenerateStaticFile = ConfigurationManager.AppSettings["EnableGenerateStaticFile"];
        public static String LinkOfProgramme_0_Title = ConfigurationManager.AppSettings["LinkOfProgramme_0_Title"];
        public static String project = ConfigurationManager.AppSettings["project"];
        public static String projectJsonName = ConfigurationManager.AppSettings["projectJsonName"];

        public static String publishing_field = ConfigurationManager.AppSettings["publishing_field"];

        public static String ImageUrlPrefix = "";
        public static String UrlPrefix = "";
        public static String JSON_v2_3_Folder = "";
        public static String EnableUploadStorage = "";
        public static String EnableUploadFTP = "";
        public static String EnableUploadBak = "";

       
        /// <summary>
        /// 上传json到Storage
        /// </summary>
        /// <param name="data"></param>
        /// <param name="connectionString"></param>
        /// <param name="containerName"></param>
        /// <param name="fileName"></param>
        /// <param name="mimeType"></param>
        /// <param name="enableUploadBak"></param>
        /// <returns></returns>
        public static String UploadFile(object data, string connectionString, string containerName, string fileName, string mimeType, bool enableUploadBak)
        {
            StringBuilder result = new StringBuilder();
            List<Task> tasks = new List<Task>();
            if (Common.EnableUploadStorage.ToLower().Trim() == "true")
            {
                Task task1 = new Task(() =>
                {
                    result.Append(Util.UploadFile(data, Common.StorageConnectionString, containerName, fileName, mimeType));
                });
                task1.Start();
                tasks.Add(task1);
            }

            if (EnableUploadBak.ToLower().Trim() == "true")
            {
                if (enableUploadBak == true)
                {
                    Task task2 = new Task(() =>
                    {
                        Util.UploadFile(data, Common.StorageConnectionStringBak, containerName, fileName, mimeType);
                    });
                    task2.Start();
                    tasks.Add(task2);
                }
            }

            if (EnableUploadFTP.ToLower().Trim() == "true")
            {
                Task task3 = new Task(() =>
                {
                    FTPUtil.UploadFile(data, Common.FTPStorageHost, Common.FTPStorageUser, Common.FTPStoragePassword, containerName, fileName);

                });
                task3.Start();
                tasks.Add(task3);
            }

            Task.WaitAll(tasks.ToArray());

            return result.ToString();
        }

        /// <summary>
        /// 获取子programme名称
        /// </summary>
        /// <param name="isAuthPassword"></param>
        /// <param name="pId"></param>
        /// <param name="basicAuthPassword"></param>
        /// <returns></returns>
        public static String GetProgrammeName(int isAuthPassword, String pId, String basicAuthPassword)
        {
            String result = "";

            if (isAuthPassword == 0)
            {
                result = "programme_" + pId + ".json";
            }
            else
            {
                String EncodeValue = ValueConvert.GetHMACSHA1Value(basicAuthPassword, pId);
                result = Common.EncryptProgrammeJsonNameFormat.Replace("12345", pId).Replace("32e9c1585a8527160951a580226ef9ad69b4667c", EncodeValue);
            }

            return result;
        }
        public static int CheckIntNull(object obj)
        {
            int result = 0;
            int.TryParse(obj.ToString(), out result);
            return result; ;
        }

        public static object CheckStringNull(object obj)
        {
            return obj == null ? "" : obj;
        }

        public static object ConvertToSize(int width, int height)
        {
            return new { width = width, height = height };
        }

        public static List<Dictionary<string, object>> programmesList = new List<Dictionary<string, object>>();
        public static SystemConfigV23 systemConfigList = new SystemConfigV23();
        public static Dictionary<string, object> rootProgramme = new Dictionary<string, object>();
        public static List<Dictionary<string, object>> programmeIdList = new List<Dictionary<string, object>>();
        public static List<string> programmesId_name = new List<string>();
        public static List<ConfigInfo> configInfoList = new List<ConfigInfo>();


        public static void SetConfigInfo()
        {
            TVBFunDao tvbfunDao = new TVBFunDao();
            Common.configInfoList = tvbfunDao.GetConfigInfoList(Common.publishing_field);
            if (configInfoList.Count > 0)
            {
                foreach (var item in configInfoList)
                {
                    ImageUrlPrefix = item.ImageUrlPrefix;
                    UrlPrefix = item.UrlPrefix;
                    JSON_v2_3_Folder = item.JSON_v2_3_Folder;
                    EnableUploadStorage = item.EnableUploadStorage == true ? "True" : "False";
                    EnableUploadFTP = item.EnableUploadFTP == true ? "True" : "False";
                    EnableUploadBak = item.EnableUploadBak == true ? "True" : "False";
                }
            }
            else
            {
                ImageUrlPrefix = ConfigurationManager.AppSettings["ImageUrlPrefix"];
                UrlPrefix = ConfigurationManager.AppSettings["UrlPrefix"];
                JSON_v2_3_Folder = ConfigurationManager.AppSettings["JSON_v2_3_Folder"];
                EnableUploadStorage = ConfigurationManager.AppSettings["EnableUploadStorage"];
                EnableUploadFTP = ConfigurationManager.AppSettings["EnableUploadFTP"];
                EnableUploadBak = ConfigurationManager.AppSettings["EnableUploadBak"];
            }
        }
    }
}