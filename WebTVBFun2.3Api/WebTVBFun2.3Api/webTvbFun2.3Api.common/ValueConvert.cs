﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace WebTVBFun2._3Api.webTvbFun2._3Api.common
{
    public class ValueConvert
    {
        public static String GetHMACSHA1Value(String password, String programId)
        {
            HMACSHA1 hmacsha1 = new HMACSHA1();
            hmacsha1.Key = Encoding.UTF8.GetBytes(password);
            byte[] dataBuffer = Encoding.UTF8.GetBytes(programId);
            byte[] hashBytes = hmacsha1.ComputeHash(dataBuffer);
            return Base16Encoder.Encode(hashBytes).ToLower();
        }

        public static int ToInt32(object obj)
        {

            try
            {
                return Convert.ToInt32(obj);
            }
            catch (Exception ex)
            {
                return default(Int32);
            }
        }


        public static String ToString(object obj)
        {
            try
            {
                return Convert.ToString(obj);
            }
            catch (Exception ex)
            {
                return default(String);
            }
        }
        public static DateTime ToDateTime(object obj)
        {
            try
            {
                return Convert.ToDateTime(obj);
            }
            catch (Exception ex)
            {
                return new DateTime(1900, 1, 1);
            }
        }
       
    }
}