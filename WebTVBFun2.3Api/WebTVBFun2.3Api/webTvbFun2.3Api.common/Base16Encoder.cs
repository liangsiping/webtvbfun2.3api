﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace WebTVBFun2._3Api.webTvbFun2._3Api.common
{
    public class Base16Encoder
    {
        private static char[] HEX = new char[]{
        '0', '1', '2', '3', '4', '5', '6', '7',
        '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

        /**
         * Convert bytes to a base16 string.
         * 将字节转换为base16字符串。
         */
        public static String Encode(byte[] byteArray)
        {
            StringBuilder hexBuffer = new StringBuilder(byteArray.Length * 2);
            for (int i = 0; i < byteArray.Length; i++)
                for (int j = 1; j >= 0; j--)
                    hexBuffer.Append(HEX[(byteArray[i] >> (j * 4)) & 0xF]);
            return hexBuffer.ToString();
        }

    }
}