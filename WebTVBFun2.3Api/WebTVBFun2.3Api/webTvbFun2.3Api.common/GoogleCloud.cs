﻿using FirebaseNetAdmin;
using FirebaseNetAdmin.Configurations.ServiceAccounts;
using Google.Apis.Auth.OAuth2;
using Google.Cloud.Firestore;
using Google.Cloud.Firestore.V1;
using Grpc.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace WebTVBFun2._3Api.webTvbFun2._3Api.common
{
    public class GoogleCloud
    {
        /// <summary>
        /// 添加systemConfig到firestore DB
        /// </summary>
        /// <param name="systemConfig"></param>
        /// <returns></returns>
        public static async Task AddSystemConfigData(Dictionary<string, Dictionary<string,object>> systemConfig)
        {
            try
            {
                string jsonPath = AppDomain.CurrentDomain.BaseDirectory + "/config/" + Common.projectJsonName;
                var jsonString = File.ReadAllText(jsonPath);
                var builder = new FirestoreClientBuilder { JsonCredentials = jsonString };
                FirestoreDb db = FirestoreDb.Create(Common.project, builder.Build());
                DocumentReference docRef = db.Collection("TVBFunCMSV2").Document("systemConfig");
                DocumentSnapshot snapshot = await docRef.GetSnapshotAsync();
                if (snapshot.Exists)
                {
                    Dictionary<string, object> city = snapshot.ToDictionary();
                    await docRef.DeleteAsync();
                    await docRef.SetAsync(systemConfig);
                    // await docRef.UpdateAsync("systemConfig", systemConfig);
                }
                else
                {
                    await docRef.SetAsync(systemConfig);
                }
            }
            catch (Exception ex)
            {

            }
           
        }
        /// <summary>
        /// 添加root programme到firestore
        /// </summary>
        /// <param name="rootProgramme"></param>
        /// <returns></returns>
        public static async Task AddRootProgrammeListData( object rootProgramme)
        {
            try
            {
                string jsonPath = AppDomain.CurrentDomain.BaseDirectory + "/config/" + Common.projectJsonName;
                var jsonString = File.ReadAllText(jsonPath);
                var builder = new FirestoreClientBuilder { JsonCredentials = jsonString };
                FirestoreDb db = FirestoreDb.Create(Common.project, builder.Build());
                DocumentReference docRef = db.Collection("TVBFunCMSV2").Document("programme_0");
                DocumentSnapshot snapshot = await docRef.GetSnapshotAsync();
                if (snapshot.Exists)
                {
                    Dictionary<string, object> city = snapshot.ToDictionary();
                    await docRef.DeleteAsync();
                    await docRef.SetAsync(rootProgramme);
                    //await docRef.UpdateAsync("programme_0", rootProgramme);
                }
                else
                {
                    await docRef.SetAsync(rootProgramme);
                }
            }
            catch (Exception ex)
            {

            }

        }
        /// <summary>
        /// 添加子programme到firestore DB
        /// </summary>
        /// <param name="programmeList"></param>
        /// <param name="documentName"></param>
        /// <returns></returns>
        public static async Task AddProgrammeListData(Dictionary<string, object> programmeList,string documentName)
        {
            try
            {
                string jsonPath = AppDomain.CurrentDomain.BaseDirectory + "/config/" + Common.projectJsonName;
                var jsonString = File.ReadAllText(jsonPath);
                var builder = new FirestoreClientBuilder { JsonCredentials = jsonString };
                FirestoreDb db = FirestoreDb.Create(Common.project, builder.Build());
                DocumentReference docRef = db.Collection("TVBFunCMSV2").Document(documentName);
                DocumentSnapshot snapshot = await docRef.GetSnapshotAsync();
                if (snapshot.Exists)
                {
                    Dictionary<string, object> city = snapshot.ToDictionary();
                    await docRef.DeleteAsync();
                    await docRef.SetAsync(programmeList);
                    //await docRef.UpdateAsync(documentName, programmeList);
                }
                else
                {
                    await docRef.SetAsync(programmeList);
                }
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// 根据programme ID删除firestore DB上的document
        /// </summary>
        /// <param name="documentName"></param>
        /// <returns></returns>
        public static async Task deleteProgrammeListData(List<string> documentName)
        {
            try
            {
                string jsonPath = AppDomain.CurrentDomain.BaseDirectory + "/config/" + Common.projectJsonName;
                var jsonString = File.ReadAllText(jsonPath);
                var builder = new FirestoreClientBuilder { JsonCredentials = jsonString };
                FirestoreDb db = FirestoreDb.Create(Common.project, builder.Build());
                foreach (var item in documentName)
                {
                    DocumentReference docRef = db.Collection("TVBFunCMSV2").Document(item);
                    DocumentSnapshot snapshot = await docRef.GetSnapshotAsync();
                    if (snapshot.Exists)
                    {
                        await docRef.DeleteAsync();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}